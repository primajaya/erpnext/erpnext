# Copyright (c) 2025, Frappe Technologies Pvt. Ltd. and contributors
# For license information, please see license.txt

import frappe
from frappe.model.document import Document

class ItemTypeRN(Document):
	pass

@frappe.whitelist()
def update_increment(type_number, inc, id):
    data = frappe.db.get_value("Item Type RN",type_number,["name","type_increment"], as_dict=True)
    if data:
        if inc==data.type_increment:
            frappe.db.set_value("Item Type RN",type_number,"type_increment",inc+1)
        else:
            datainc = data.type_increment+1
            if inc > data.type_increment:
                datainc = inc+1
                
            data = frappe.db.set_value("Item Type RN",type_number,"type_increment",datainc)
            
            ## update rn item
            # strDataInc = ""+datainc
            # pad = "0000"
            # rn = type_number + pad[0, pad.length - strDataInc.length] + strDataInc
            # frappe.db.set_value("Item",id,{"custom_registration_number":rn, "custom_inc_rn_number":data.increment})
			
   
    return data